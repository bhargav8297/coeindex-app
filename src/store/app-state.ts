import { ActiveTracker, Survey, SurveyGroup, Tracker, UserContext } from "src/services/apiService/response-models";

export interface AppState {
  surveys: Survey[];
  activeTracker: ActiveTracker | null;
  sidebarOpen: boolean;
  isDrawerOpen: boolean;
  appLoaderVisible?: boolean;
  userCtx: UserContext;
}

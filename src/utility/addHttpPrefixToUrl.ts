export const addHttpPrefixToUrl = (url: string) => {
    const lowerCaseUrl = url.toLowerCase();
    if (lowerCaseUrl.startsWith('http://') || lowerCaseUrl.startsWith('https://')) {
        return lowerCaseUrl;
    }
    return `http://${lowerCaseUrl}`;
}
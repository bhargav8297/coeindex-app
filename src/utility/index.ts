export const readPortalNameFromUrl = (): string => {
  const pathArray = window.location.pathname
    .split('/')
    .filter((o) => o.trim().length);
  return pathArray.length && pathArray[0].toLowerCase() !== 'not-found'
    ? pathArray[0]
    : '';
};

export const getTextColorByTheme = (theme: string): string => {
  if (theme === 'light') {
    return 'text-black';
  }
  return 'text-white';
}

// javasciprt validation for text allow only alpahaNumeric, dot, dash, underscrore
export const validateProfileText = (value: string): boolean => {
  const regex = /^[a-zA-Z0-9_.-]*$/;
  return regex.test(value);
}
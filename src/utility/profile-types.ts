enum ProfileTypes {
  'Business' = 'Company',
  'Individual' = 'Member',
}

export default ProfileTypes;
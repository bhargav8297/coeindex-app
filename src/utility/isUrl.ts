export default function isUrl(txt?: string | null): boolean {
    if (!txt)
        return false;

    const val = txt.trim().toLowerCase();

    const validStarts = ['http://', 'https://', 'www.'];

    let isValid = false;

    validStarts.forEach(o => {
        if (!isValid && val.startsWith(o)) {
            isValid = true;
        }
    });
     

    return isValid;
}
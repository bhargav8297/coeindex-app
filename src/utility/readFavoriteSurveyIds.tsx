const readFavoriteSurveyIds = (): number[] => {
  if (localStorage.getItem('favIds'))
    return JSON.parse(localStorage.getItem('favIds'));
  return [];
};

export default readFavoriteSurveyIds;

 const getFileExt = (fileName: string): string => {
        const parts = fileName.split('.');
        return parts.length > 1 ? parts[parts.length - 1] : '';
};

export default getFileExt;


export default function textToEllipsis(txt?: string | null, maxLength: number = 50): string {
    if (!txt)
        return '';

    if (txt.length > maxLength) {
        return txt.substr(0, maxLength) + '...';
    }
    return txt;
}

enum Constants {
  DEFAULT_TEAM_NAME = 'Default team'
}


export const USER_ROLES = [
  { label: 'Administrator', roleName: 'admin' },
  { label: 'Manager', roleName: 'manager' },
  { label: 'Member', roleName: 'member' }
];

export default Constants;

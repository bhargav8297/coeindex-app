const getSubDomain = (): string => {
  const parts = window.location.host.split('.');
  let mainDomainPartCount = 2;
  const isLocalhost = parts[parts.length - 1].indexOf('localhost') === 0;
  if (isLocalhost) mainDomainPartCount = 1;
  const subDomains = parts.slice(0, parts.length - mainDomainPartCount);
  return subDomains.length ? subDomains[0] : '';
};

export default getSubDomain;

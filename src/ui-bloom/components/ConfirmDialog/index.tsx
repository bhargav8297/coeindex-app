import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  CircularProgress
} from '@mui/material';
import { FunctionComponent } from 'react';

export interface ConfirmDialogProps {
  title: string;
  description: string;
  onCancel: () => void;
  onOk: () => void;
  okText?: string;
  cancelText?: string;
  isOpen: boolean;
  loading?: boolean;
}

const ConfirmDialog: FunctionComponent<ConfirmDialogProps> = (props) => {
  return (
    <>
      <Dialog
        open={props.isOpen}
        onClose={(): void => props.onCancel()}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{props.title}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {props.description}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={(): void => props.onOk()}>
            {props.loading && (
              <CircularProgress
                size={20}
                sx={{ marginRight: '5px', marginLeft: 0 }}
              />
            )}
            {props.okText || 'OK'}
          </Button>
          <Button onClick={(): void => props.onCancel()} autoFocus>
            {props.cancelText || 'CANCEL'}
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default ConfirmDialog;

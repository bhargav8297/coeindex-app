import { useContext, type ReactNode } from 'react';

import AddTwoToneIcon from '@mui/icons-material/AddTwoTone';
import AppRegistrationIcon from '@mui/icons-material/AppRegistration';
import CardMembershipTwoToneIcon from '@mui/icons-material/CardMembershipTwoTone';
import ContactsTwoToneIcon from '@mui/icons-material/ContactsTwoTone';
import GroupAddTwoToneIcon from '@mui/icons-material/GroupAddTwoTone';
import GroupsTwoToneIcon from '@mui/icons-material/GroupsTwoTone';
import ListAltTwoToneIcon from '@mui/icons-material/ListAltTwoTone';
import PeopleOutlineTwoToneIcon from '@mui/icons-material/PeopleOutlineTwoTone';
import PortraitTwoToneIcon from '@mui/icons-material/PortraitTwoTone';
import RateReviewIcon from '@mui/icons-material/RateReview';
import SettingsTwoToneIcon from '@mui/icons-material/SettingsTwoTone';
import StorefrontIcon from '@mui/icons-material/Storefront';
import StorefrontTwoToneIcon from '@mui/icons-material/StorefrontTwoTone';
import UploadTwoToneIcon from '@mui/icons-material/UploadTwoTone';
import HomeIcon from '@mui/icons-material/Home';
import { Group } from '@mui/icons-material';
import { Badge } from '@mui/material';


export interface MenuItem {
  link?: string;
  icon?: ReactNode;
  badge?: any;
  badgeTooltip?: string;

  items?: MenuItem[];
  name: string;
}

export interface MenuItems {
  items: MenuItem[];
  heading: string;
}


const notific=3

export const menuItemsForAdmin: MenuItems[] = [
  {
    heading: 'Dashboards',
    items: [
      // {
      //   name: 'Main',
      //   icon: StorefrontTwoToneIcon,
      //   link: '/app/dashboard'
      // },
   
      {
        name: 'Home',
        link: '/app/dashboard',
        icon: HomeIcon,
      }
    ]
  },
  {
    heading: 'List',
    items:[
      {
        name: 'List',
        icon: ListAltTwoToneIcon,
        link: '/app/assessmentslist'
      },

    ]
  },
  // {
  //   heading: 'BUSINESS',
  //   items: [
  //     {
  //       name: 'Surveys',
  //       icon: ContactsTwoToneIcon,
  //       link: '',
  //       items: [
  //         {
  //           name: 'Data Modelling',
  //           icon: ListAltTwoToneIcon,
  //           link: '/app/survey/data-modelling'
  //         },
  //         {
  //           name: 'Data Security',
  //           icon: ListAltTwoToneIcon,
  //           link: '/app/survey/data-security'
  //         },
  //       ]
  //     },
  //     {
  //       name: 'Reviews',
  //       icon: GroupAddTwoToneIcon,
  //       link: '',
  //       items: [
  //         {
  //           name: 'Reviews',
  //           icon: RateReviewIcon,
  //           link: '/app/review/list'
  //         },
  //         {
  //          name: 'New',
  //          icon: AddTwoToneIcon,
  //          link: '/app/lead/new'
  //         }
  //       ]
  //     }
  //   ]
  // },
  {
    heading: 'User Approval',
    items:[
      {
        name: 'UserApproval',
        icon: Group,
        link: '/app/usermanagement',
        badge: notific,
        badgeTooltip: 'New user approvals'
        }
    ]
  },
  {
    heading: 'MANAGE',
    items: [
      {
        name: 'Setting',
        icon: SettingsTwoToneIcon,
        link: '/app/setting'
      }
    ]
  }
];

export const menuItemsForNonAdminUser: MenuItems[] = [
  {
    heading: 'Dashboards',
    items: [
      {
        name: 'Main',
        icon: StorefrontTwoToneIcon,
        link: '/app/dashboard'
      }
    ]
  },
  {
    heading: 'MANAGEMENT',
    items: [
      {
        name: 'Company Profiles',
        icon: ListAltTwoToneIcon,
        link: '/app/cards/business'
      },
      {
        name: 'Member Profiles',
        icon: ListAltTwoToneIcon,
        link: '/app/cards/personal'
      },
      {
        name: 'My Cards',
        icon: CardMembershipTwoToneIcon,
        link: '/app/my-cards'
      }
    ]
  },
  {
    heading: 'BUSINESS',
    items: [
      {
        name: 'Contact',
        icon: ContactsTwoToneIcon,
        link: '',
        items: [
          {
            name: 'List',
            icon: ListAltTwoToneIcon,
            link: '/app/contact/list'
          },
          {
            name: 'New',
            icon: AddTwoToneIcon,
            link: '/app/contact/new'
          },
          {
            name: 'Upload',
            icon: UploadTwoToneIcon,
            link: '/app/contact/upload'
          }
        ]
      },
      {
        name: 'Lead',
        icon: GroupAddTwoToneIcon,
        link: '',
        items: [
          {
            name: 'List',
            icon: ListAltTwoToneIcon,
            link: '/app/lead/list'
          }
        ]
      },
      {
        name: 'Reviews',
        icon: GroupAddTwoToneIcon,
        link: '',
        items: [
          {
            name: 'Reviews',
            icon: RateReviewIcon,
            link: '/app/review/list'
          }
          //{
          //  name: 'New',
          //  icon: AddTwoToneIcon,
          //  link: '/app/lead/new'
          //}
        ]
      }
    ]
  }
];

export const menuItemsForIndividualUser: MenuItems[] = [
  {
    heading: 'Dashboards',
    items: [
      {
        name: 'Main',
        icon: HomeIcon,
        link: '/app/dashboard'
      }
    ]
  },
  {
    heading: 'List',
    items:[
      {
        name: 'List',
        icon: ListAltTwoToneIcon,
        link: '/app/assessmentslist'
      },

    ]
  },
  {
    heading: 'Profiles',
    items: [
      {
        name: 'Member Profiles',
        icon: PortraitTwoToneIcon,
        link: '/app/cards/personal'
      }, 
      // {
      //   name: 'My Cards',
      //   icon: CardMembershipTwoToneIcon,
      //   link: '/app/my-cards'
      // }
    ]
  },
  {
    heading: 'MANAGE',
    items: [
      // {
      //   name: 'Contact',
      //   icon: ContactsTwoToneIcon,
      //   link: '',
      //   items: [
      //     {
      //       name: 'List',
      //       icon: ListAltTwoToneIcon,
      //       link: '/app/contact/list'
      //     },
      //     {
      //       name: 'New',
      //       icon: AddTwoToneIcon,
      //       link: '/app/contact/new'
      //     },
      //     {
      //       name: 'Upload',
      //       icon: UploadTwoToneIcon,
      //       link: '/app/contact/upload'
      //     }
      //   ]
      // },
      // {
      //   name: 'Lead',
      //   icon: GroupAddTwoToneIcon,
      //   link: '',
      //   items: [
      //     {
      //       name: 'List',
      //       icon: ListAltTwoToneIcon,
      //       link: '/app/lead/list'
      //     }
      //   ]
      // },
      {
        name: 'Setting',
        icon: SettingsTwoToneIcon,
        link: '/app/setting'
      }
      //   {
      //     name: 'Reviews',
      //     icon: GroupAddTwoToneIcon,
      //     link: '',
      //     items: [
      //         {
      //             name: 'Reviews',
      //             icon: RateReviewIcon,
      //             link: '/app/review/list'
      //         },
      //         //{
      //         //  name: 'New',
      //         //  icon: AddTwoToneIcon,
      //         //  link: '/app/lead/new'
      //         //}
      //     ]
      // },
    ]
  }
];





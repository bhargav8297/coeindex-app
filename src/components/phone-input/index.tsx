import { Box, TextField, Autocomplete, styled, Popper } from '@mui/material';
import { useField } from 'formik';
import { useState } from 'react';
import { countryCodes } from '../../utility/countryCode';

const PopperWrap = styled(Popper)(
  ({ theme }) => `
  `
);

interface Phone {
  countryCode?: string;
  number: string;
}
function PhoneInput(props: { name: string; label?: string }) {
  const [field, meta, helpers] = useField(props.name);
  const [phone, setPhone] = useState<Phone>(
    field.value || { countryCode: '', number: '' }
  );

  return (
    <div style={{ width: '100%' }}>
      <Box display="flex" flexDirection="row" p={1} m={1}>
        <Autocomplete
          fullWidth
          id="country-select-demo"
          disablePortal
          options={countryCodes}
          sx={{ width: 160 }}
          autoHighlight
          value={
            field?.value
              ? countryCodes.find((o) => o.dial_code === field.value)
              : null
          }
          onChange={(
            _event,
            value: {
              name: string;
              code: string;
              dial_code: string;
            }
          ) => {
            setPhone({ ...phone, countryCode: value?.dial_code });
          }}
          getOptionLabel={(option: {
            name: string;
            code: string;
            dial_code: string;
          }) => `+${option.dial_code}`}
          renderOption={(props, option) => (
            <Box
              component="li"
              sx={{ '& > img': { mr: 2, flexShrink: 0 } }}
              {...props}
            >
              <img
                loading="lazy"
                width="20"
                src={`https://flagcdn.com/w20/${option.code.toLowerCase()}.png`}
                srcSet={`https://flagcdn.com/w40/${option.code.toLowerCase()}.png 2x`}
                alt=""
              />
              {option.name} {option.dial_code}
            </Box>
          )}
          renderInput={(params) => (
            <div ref={params.InputProps.ref}>
              <TextField
                {...params}
                label="Country code1"
                style={{ width: 400 }}
                inputProps={{
                  ...params.inputProps
                }}
              />
            </div>
          )}
        />
        <TextField
          type="tel"
          fullWidth
          sx={{ ml: 1 }}
          label={props.label || 'Phone number'}
          variant="outlined"
        />
      </Box>
    </div>
  );
}

export default PhoneInput;

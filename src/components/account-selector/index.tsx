import {
  Autocomplete,
  Card,
  Divider,
  Box,
  Grid,
  Stack,
  styled,
  TextField,
  useTheme
} from '@mui/material';
import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import AccountBalanceTwoToneIcon from '@mui/icons-material/AccountBalanceTwoTone';

const SearchIconWrapper = styled(AccountBalanceTwoToneIcon)(
  ({ theme }) => `
        color: ${theme.colors.alpha.black[30]}
`
);

function AccountSelectorOLD(props: {
  selectedIds: number[];
  disabled: boolean;
}) {
  const { t }: { t: any } = useTranslation();
  const theme = useTheme();

 
  return (
    <Grid item xs={12}>
      <Card
        sx={{
          display: 'flex',
          alignItems: 'center'
        }}
      >
        <Box display={{ xs: 'none', lg: 'flex' }} ml={2} flexShrink={1}>
          <SearchIconWrapper />
        </Box>
        <Stack
          sx={{
            p: 2,
            flex: 1
          }}
          direction={{ xs: 'column', md: 'row' }}
          justifyContent="space-evenly"
          alignItems="center"
          spacing={2}
          divider={<Divider orientation="vertical" flexItem />}
        >
         
        </Stack>
      </Card>
    </Grid>
  );
}

export default AccountSelectorOLD;

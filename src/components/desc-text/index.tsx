function DescText({ text, maxLen }: { text?: string; maxLen?: number }) {
  if (text && maxLen && text.length > maxLen) {
    return <>{text.substring(0, maxLen)}...</>;
  }
  return <>{text}</>;
}

export default DescText;

import React from 'react';
import { CircularProgress, Theme, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { spacing } from '@mui/system';

declare module '@mui/styles/defaultTheme' {
  interface DefaultTheme extends Theme {}
}
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: '100vh',
    padding: theme.spacing(2),
  },
  spinner: {
    marginBottom: theme.spacing(2),
  },
}));

const AuthenticatingLoader: React.FC = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CircularProgress className={classes.spinner} />
      <Typography variant="h6">Authenticating...</Typography>
    </div>
  );
};

export default AuthenticatingLoader;

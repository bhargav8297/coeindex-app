import { FC, useContext, useRef, useState, MouseEvent } from 'react';
import { styled, Box, Tooltip } from '@mui/material';
import Fab from '@mui/material/Fab';
import { useTranslation } from 'react-i18next';
import PlayLessonTwoToneIcon from '@mui/icons-material/PlayLessonTwoTone';
import { useNavigate } from 'react-router-dom';

const ThemeSettingsButton = styled(Box)(
  ({ theme }) => `
          position: fixed;
          z-index: 9999;
          right: ${theme.spacing(4)};
          bottom: ${theme.spacing(4)};
          
          &::before {
              width: 30px;
              height: 30px;
              content: ' ';
              position: absolute;
              border-radius: 100px;
              left: 13px;
              top: 13px;
              background: ${theme.colors.primary.main};
              animation: ripple 1s infinite;
              transition: ${theme.transitions.create(['all'])};
          }

          .MuiSvgIcon-root {
            animation: pulse 1s infinite;
            transition: ${theme.transitions.create(['all'])};
          }
  `
);

const ActiveSessionButton: FC = () => {
  const { t }: { t: any } = useTranslation();
  const navigate = useNavigate();

  const handleOpen = (): void => {
    const surveyId = 35;
    navigate('/app/active-eval/');
  };

  return (
    <>
      <ThemeSettingsButton>
        <Tooltip arrow title={t('Continue Evaluation Session')}>
          <Fab onClick={handleOpen} color="primary" aria-label="add">
            <PlayLessonTwoToneIcon />
          </Fab>
        </Tooltip>
      </ThemeSettingsButton>
    </>
  );
};

export default ActiveSessionButton;

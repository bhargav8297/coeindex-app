import {
  Card,
  Box,
  Divider,
  CardHeader,
  styled,
  Avatar,
  useTheme,
  ListItem,
  ListItemText,
  Typography,
  List,
  Alert,
  CircularProgress,
  CircularProgressProps,
  FormHelperText,
  Link,
  Stack,
  IconButton,
  ButtonGroup,
  Button
} from '@mui/material';
import { useDropzone } from 'react-dropzone';
import { useTranslation } from 'react-i18next';
import CheckTwoToneIcon from '@mui/icons-material/CheckTwoTone';
import CloudUploadTwoToneIcon from '@mui/icons-material/CloudUploadTwoTone';
import CloseTwoToneIcon from '@mui/icons-material/CloseTwoTone';
import { useCallback, useEffect, useRef, useState } from 'react';
import { useSnackbar } from 'notistack';
import cdnService from '../../services/cdnService';
import ReactCrop, { Crop, PixelCrop, defaultCrop } from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import DoneAllOutlinedIcon from '@mui/icons-material/DoneAllOutlined';
import LoadingButton from '@mui/lab/LoadingButton';
import DeleteIcon from '@mui/icons-material/Delete';

const BoxUploadWrapper = styled(Box)(
  ({ theme }) => `
    border-radius: ${theme.general.borderRadius};
    padding: ${theme.spacing(2)};
    background: ${theme.colors.alpha.black[5]};
    border: 1px dashed ${theme.colors.alpha.black[30]};
    outline: none;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    transition: ${theme.transitions.create(['border', 'background'])};

    &:hover {
      background: ${theme.colors.alpha.white[50]};
      border-color: ${theme.colors.primary.main};
    }
`
);

const AvatarWrapper = styled(Avatar)(
  ({ theme }) => `
    background: transparent;
    color: ${theme.colors.primary.main};
    width: ${theme.spacing(7)};
    height: ${theme.spacing(7)};
`
);

const AvatarSuccess = styled(Avatar)(
  ({ theme }) => `
    background: ${theme.colors.success.light};
    width: ${theme.spacing(7)};
    height: ${theme.spacing(7)};
`
);

const AvatarDanger = styled(Avatar)(
  ({ theme }) => `
    background: ${theme.colors.error.light};
    width: ${theme.spacing(7)};
    height: ${theme.spacing(7)};
`
);

const initCrop: Crop = {
    x: 5,
    y: 5,
    width: 80,
    height: 80,
    unit: '%'
}

function CircularProgressWithLabel(
  props: CircularProgressProps & { value: number }
) {
  return (
    <Box mt={1} sx={{ position: 'relative', display: 'inline-flex' }}>
      <CircularProgress variant="determinate" {...props} />
      <Box
        sx={{
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          position: 'absolute',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center'
        }}
      >
        <Typography
          variant="caption"
          component="div"
          color="text.secondary"
        >{`${Math.round(props.value)}%`}</Typography>
      </Box>
    </Box>
  );
}
function FeatureImageCard(props: {
  title: string;
  uploadedFileName?: string;
  isSubmit?: boolean;
    onChange: (fileName: string) => void;
    isRequired?: boolean;

}) {
  const { t }: { t: any } = useTranslation();
  const { enqueueSnackbar } = useSnackbar();

  const [isUploading, setIsUploading] = useState(false);
  const [isCropInProgress, setIsCropInProgress] = useState(false);
  const [isSubmit, setIsSubmit] = useState(false);
  const [isFileUploaded, setIsFileUploaded] = useState(
    props.uploadedFileName?.length > 0
  );
  const [isEmpty, setIsEmpty] = useState(!(props.uploadedFileName?.length > 0));
  const [uploadedFileName, setUploadedFileName] = useState(
    props.uploadedFileName || ''
  );
  const [progress, setProgress] = useState(0);

  const [imgSrc, setImgSrc] = useState('');
  const [crop, setCrop] = useState<Crop>();
  const [file, setFile] = useState<File | null>(null);
  const [completedCrop, setCompletedCrop] = useState<PixelCrop>();
  const previewCanvasRef = useRef<HTMLCanvasElement>(null);

  let progressTimerRef: any;

  useEffect(() => {
    setIsSubmit(props.isSubmit);
  }, [props.isSubmit]);

    useEffect(() => {
        setIsEmpty(!props.uploadedFileName?.length);
      setIsFileUploaded(props.uploadedFileName?.length > 0);
      setUploadedFileName(props.uploadedFileName);
  }, [props.uploadedFileName]);

  const onDropAccepted = useCallback((acceptedFiles) => {
    setIsEmpty(false);
    setIsCropInProgress(true);
    setCrop(undefined);

    const file = acceptedFiles[0];
    setFile(file);
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      setImgSrc(reader.result?.toString() || '');
        setCrop(initCrop);
    });
    reader.readAsDataURL(file);
    //uploadFileToCloud(file).then(blobName => {
    //    if (blobName) {
    //        (file as any).blobName = blobName;
    //        props.onChange(file);
    //    }
    //});
  }, []);

  function getCroppedImg(image: any, pixelCrop: PixelCrop) {
    const canvas = document.createElement('canvas');
    canvas.width = pixelCrop.width;
    canvas.height = pixelCrop.height;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
      image,
      pixelCrop.x,
      pixelCrop.y,
      pixelCrop.width,
      pixelCrop.height,
      0,
      0,
      pixelCrop.width,
      pixelCrop.height
    );

    // As Base64 string
    // const base64Image = canvas.toDataURL('image/jpeg');

    // As a blob
    return new Promise((resolve, reject) => {
      canvas.toBlob((file) => {
        //  file..name = fileName;
        resolve(file);
      }, 'image/jpeg');
    });
  }

  const onCropComplete = async () => {
    setIsUploading(true);
    setIsCropInProgress(false);

    var img = new Image();
      img.onload = function () {

          let pxCrop = completedCrop;
          if (!pxCrop) {
              pxCrop = {
                  x:initCrop.x,
                  y: initCrop.y,
                  unit:'px',
                  width: img.width * initCrop.width / 100,
                  height: img.height * initCrop.height / 100,
              }
          }
          getCroppedImg(img, pxCrop).then((blob) => {
        uploadFileToCloud(file.name, blob).then((blobName) => {
          if (blobName) {
            (file as any).blobName = blobName;
            setUploadedFileName(blobName);
              props.onChange(blobName);
          }
        });
      });
    };
    img.src = URL.createObjectURL(file);
  };

  const onRemove = () => {
    setUploadedFileName('');
    setIsFileUploaded(false);
    setFile(null);
      setIsEmpty(true);
      props.onChange('');

  };

  const {
    isDragActive,
    isDragAccept,
    isDragReject,
    getRootProps,
    getInputProps
  } = useDropzone({
    maxFiles: 1,
    maxSize: 5242880,
    accept: {
      'image/png': ['.png'],
      'image/jpeg': ['.jpg']
    },
    onDropAccepted,
    disabled: isUploading
  });

  const startTimer = () => {
    setIsUploading(true);
    setProgress(0);
    progressTimerRef = setInterval(() => {
      setProgress((prevProgress) =>
        prevProgress >= 90 ? prevProgress : prevProgress + 10
      );
    }, 200);
  };

  const uploadFileToCloud = async (
    fileName: string,
    blob: any
  ): Promise<string> => {
    let blobName = '';
    try {
      const getExt = (fileName: string): string => {
        const parts = fileName.split('.');
        return parts.length > 1 ? parts[parts.length - 1] : '';
      };

      startTimer();
      const ext = getExt(fileName);

      blobName = 'img-' + new Date().getTime() + (ext ? '.' + ext : '');

      const containerClient = cdnService.getContainerClient(
        cdnService.imageContainerName
      );
      const blockBlobClient = containerClient.getBlockBlobClient(blobName);
      const res = await blockBlobClient.uploadBrowserData(blob, {
        onProgress: (o) => {
          //    total +=   o.loadedBytes;
        }
      });
      setProgress(100);

      setTimeout((o) => {
        setIsUploading(false);
        setIsFileUploaded(true);
      }, 2000);
    } catch (error) {
      console.log(error);
      enqueueSnackbar(t(`Failed to upload image`), { variant: 'error' });
      setIsUploading(false);
      setIsFileUploaded(false);
    }
    clearInterval(progressTimerRef);
    return blobName;
  };

  //const files = acceptedFiles.map((file, index) => (
  //    <ListItem disableGutters component="div" key={index}>
  //        <ListItemText primary={file.name} />
  //        <b>{file.size} bytes</b>
  //        <Divider />
  //    </ListItem>
  //));

  return (
    <Card
      sx={{
        m: 0
      }}
    >
      <CardHeader title={t(props.title)} />
      <Divider />
      <Box p={2}>
        {isEmpty && (
          <BoxUploadWrapper {...getRootProps()}>
            <input {...getInputProps()} />
            {isDragAccept && (
              <>
                <AvatarSuccess variant="rounded">
                  <CheckTwoToneIcon />
                </AvatarSuccess>
                <Typography
                  sx={{
                    mt: 2
                  }}
                >
                  {t('Drop the files to start uploading')}
                </Typography>
              </>
            )}
            {isDragReject && (
              <>
                <AvatarDanger variant="rounded">
                  <CloseTwoToneIcon />
                </AvatarDanger>
                <Typography
                  sx={{
                    mt: 2
                  }}
                >
                  {t('You cannot upload these file types')}
                </Typography>
              </>
            )}
            {!isDragActive && (
              <>
                <AvatarWrapper variant="rounded">
                  <CloudUploadTwoToneIcon />
                </AvatarWrapper>
                <Typography
                  sx={{
                    mt: 2
                  }}
                >
                  {t('Drag & drop file here or browse')}
                </Typography>
              </>
            )}
          </BoxUploadWrapper>
        )}
        {!isEmpty && !isFileUploaded && (
          <BoxUploadWrapper sx={{ p: 1 }}>
            {isDragAccept && (
              <>
                <AvatarSuccess variant="rounded">
                  <CheckTwoToneIcon />
                </AvatarSuccess>
                <Typography
                  sx={{
                    mt: 2
                  }}
                >
                  {t('Drop the files to start uploading')}
                </Typography>
              </>
            )}
            {isDragReject && (
              <>
                <AvatarDanger variant="rounded">
                  <CloseTwoToneIcon />
                </AvatarDanger>
                <Typography
                  sx={{
                    mt: 2
                  }}
                >
                  {t('You cannot upload these file types')}
                </Typography>
              </>
            )}
            {!isCropInProgress && !isDragActive && (
              <>
                <AvatarWrapper variant="rounded">
                  <CloudUploadTwoToneIcon />
                </AvatarWrapper>
                <Typography
                  sx={{
                    mt: 2
                  }}
                >
                  {isUploading
                    ? t('Uploading...')
                    : t('Drag & drop file here or browse')}
                </Typography>
              </>
            )}

            {isUploading && <CircularProgressWithLabel value={progress} />}

            {isCropInProgress && (
                          <>
                              <img src={imgSrc} width="100%" height="100%" />

                              {
                              //    <ReactCrop
                              //    disabled={true}
                              //    crop={crop}
                              //    onChange={(_, percentCrop) => setCrop(percentCrop)}
                              //    onComplete={(c) => setCompletedCrop(c)}
                              //>
                              //</ReactCrop>
                              }
                <ButtonGroup
                  variant="contained"
                  sx={{ mt: 1 }}
                  size="small"
                  aria-label="outlined button group"
                >
                  <Button
                    onClick={() => setIsEmpty(true)}
                    color="error"
                    variant="outlined"
                  >
                    Cancel
                  </Button>
                  <LoadingButton
                    onClick={() => onCropComplete()}
                    loading={false}
                    loadingIndicator="Loading�"
                    variant="outlined"
                  >
                    Confirm
                  </LoadingButton>
                </ButtonGroup>
              </>
            )}
          </BoxUploadWrapper>
        )}
              { isFileUploaded && (
          <BoxUploadWrapper sx={{ p: 1 }}>
            <img
              src={cdnService.toImageUrl(uploadedFileName)}
              width="100%"
              height="100%"
            />
          </BoxUploadWrapper>
        )}
      </Box>

      {isFileUploaded && !isUploading && (
        <>
          <Divider />
          <Box p={1} textAlign="center">
            <Link
              href="#"
              onClick={(e) => {
                e.preventDefault();
                onRemove();
              }}
              underline="none"
              color="error"
            >
              {'Remove'}
            </Link>
          </Box>
        </>
      )}
          {props.isRequired === true && isSubmit === true && !isFileUploaded && (
        <Box p={2} pt={0}>
          <FormHelperText error={true}>
            The feature image is required
          </FormHelperText>
        </Box>
      )}
    </Card>
  );
}

export default FeatureImageCard;

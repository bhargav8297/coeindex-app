import React, { useState, useEffect } from 'react';
import {
  Button,
  Card,
  CardContent,
  Grid,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
  useMediaQuery,
  Box,
  useTheme,
  Checkbox,
  IconButton,
  Skeleton,
  AlertColor,
  TablePagination,
  Container
} from '@mui/material';
import { downloadSurveyPDF, getAllSurveyGroups } from 'src/services/apiService';
import { Survey } from 'src/services/apiService/response-models';
import { CheckBoxOutlineBlankOutlined, CheckBoxOutlined } from '@mui/icons-material';
import _ from 'lodash';
import { RootState, useSelector } from 'src/store';
import { useTranslation } from 'react-i18next';
import PageHeader from './PageHeader';
import AlertSnackbar from './AlertSnackbar';
import { Helmet } from 'react-helmet-async';

const AssessmentList: React.FC = () => {
  const [surveys, setSurveys] = useState<Survey[]>([]);
  const [selectAll, setSelectAll] = useState(false);
  const [selectedSurveys, setSelectedSurveys] = useState<number[]>([]);
  const [dataFetched, setDataFetched] = useState(false);
  const [page, setPage] = useState<number>(0);
  const [limit, setLimit] = useState<number>(20); // Default limit
  const isExtraLargeScreen = useMediaQuery('(min-width: 2560px)');
  const isSmallScreen = useMediaQuery('(max-width: 600px)');
  const { userCtx } = useSelector((state: RootState) => state.app);
  const userId = userCtx.userId;
  const [alertOpen, setAlertOpen] = useState(false);
  const [alertSeverity, setAlertSeverity] = useState<AlertColor>('success');
  const [alertMessage, setAlertMessage] = useState('');
  const { t }: { t: any } = useTranslation();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await getAllSurveyGroups();
        const surveys = _.flatten(res.data.map((o) => o.surveys));
        setSurveys(surveys);
        setDataFetched(true);
      } catch (error) {
        console.error('Error fetching assessments:', error);
      }
    };

    fetchData();
  }, []);

  const handleDownload = async (surveyId: number, surveyName: string, userId: number) => {
    try {
      const pdfBlob = await downloadSurveyPDF(surveyId, surveyName, userId);
      if (!pdfBlob) {
        console.error(`PDF not found for survey ${surveyName}`);
        setAlertSeverity('error');
        setAlertMessage(`PDF not found for survey ${surveyName}`);
        setAlertOpen(true);
        return;
      }
      const url = window.URL.createObjectURL(new Blob([pdfBlob]));
      const a = document.createElement('a');
      a.href = url;
      a.download = `survey_${surveyName}.pdf`; // Use surveyName for file name
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);
    } catch (error) {
      console.error(`Error downloading survey ${surveyName}:`, error);
      setAlertSeverity('error');
      setAlertMessage(`PDF not found for survey ${surveyName}`);
      setAlertOpen(true);
    }
  };

  const handleSelectAll = () => {
    const allSurveyIds = surveys.map(survey => survey.surveyID);
    if (selectAll) {
      setSelectedSurveys([]);
    } else {
      setSelectedSurveys(allSurveyIds);
    }
    setSelectAll(!selectAll);
  };

  const handleDownloadAll = async () => {
    try {
      for (const surveyId of selectedSurveys) {
        const survey = surveys.find(survey => survey.surveyID === surveyId);
        if (survey) {
          await handleDownload(survey.surveyID, survey.surveyName, userId);
        }
      }
    } catch (error) {
      console.error('Error downloading all surveys:', error);
    }
  };

  const handleCheckboxChange = (surveyId: number) => {
    if (selectedSurveys.includes(surveyId)) {
      setSelectedSurveys(prevState => prevState.filter(id => id !== surveyId));
    } else {
      setSelectedSurveys(prevState => [...prevState, surveyId]);
    }
  };

  const handlePageChange = (event: any, newPage: number) => {
    setPage(newPage);
  };

  const handleLimitChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setLimit(parseInt(event.target.value, 10));
    setPage(0); // Reset to first page when changing the limit
  };

  // Slice surveys based on current page and limit
  const paginatedSurveys = surveys.slice(page * limit, page * limit + limit);

  return (
    <Container maxWidth={isExtraLargeScreen ? 'xl' : 'lg'}>
      <Helmet>
        <title>Assessments List</title>
      </Helmet>
      <PageHeader mobile={isSmallScreen} />
      {!dataFetched ? (
        <Card>
          <CardContent>
            {[...Array(10)].map((_, index) => (
              <List key={index}>
                <ListItem>
                  <Skeleton variant="rectangular" width="100%" height={60} />
                </ListItem>
              </List>
            ))}
          </CardContent>
        </Card>
      ) : (
        <Card>
          <CardContent>
            <List>
              <ListItem sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                <IconButton onClick={handleSelectAll} sx={{ p: 1 }}>
                  {selectAll ? <CheckBoxOutlined /> : <CheckBoxOutlineBlankOutlined />}
                </IconButton>
                <ListItemText
                  primary="Select All"
                  primaryTypographyProps={{ variant: 'body1', sx: { fontWeight: 'bold', fontSize: isSmallScreen ? '0.8rem' : '1rem', color: 'textPrimary' } }}
                />
                <Button
                  variant="contained"
                  color="primary"
                  size="small"
                  onClick={handleDownloadAll}
                  disabled={selectedSurveys.length <= 1}
                  sx={{ fontSize: isSmallScreen ? '0.7rem' : '0.8rem', minWidth: '90px' }}
                >
                  {t('Download All')}
                </Button>
              </ListItem>
              {paginatedSurveys.map((survey, index) => (
                <Grid item key={survey.surveyID} xs={12}>
                  <ListItem
                    sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', '&:hover': { backgroundColor: '#f5f5f5' }, mb: 1 }}
                    onMouseEnter={() => console.log('Hovered over survey:', survey.surveyID)}
                  >
                    <Checkbox
                      checked={selectedSurveys.includes(survey.surveyID)}
                      onChange={() => handleCheckboxChange(survey.surveyID)}
                      sx={{ ml: 0, mr: 1, '& .MuiSvgIcon-root': { fontSize: '1rem' } }}
                    />
                    <ListItemText
                      primary={survey.surveyName}
                      primaryTypographyProps={{ variant: 'body1', sx: { fontWeight: 'bold', fontSize: isSmallScreen ? '0.8rem' : '1rem', color: 'textPrimary' } }}
                    />
                    <Box sx={{ flex: '0 0 auto', minWidth: '90px', ml: isSmallScreen ? 1 : 0 }}>
                      <Button
                        onClick={() => handleDownload(survey.surveyID, survey.surveyName, userId)}
                        variant="contained"
                        color="primary"
                        size="small"
                        sx={{ width: '100%', fontSize: isSmallScreen ? '0.7rem' : '0.8rem' }}
                      >
                        {t('Download')}
                      </Button>
                    </Box>
                  </ListItem>
                </Grid>
              ))}
            </List>
          </CardContent>
        </Card>
      )}
      <Box mt={2} />
      <Card>
        <CardContent>
          <TablePagination
            component="div"
            count={surveys.length}
            page={page}
            onPageChange={handlePageChange}
            rowsPerPage={limit}
            onRowsPerPageChange={handleLimitChange}
          />
        </CardContent>
      </Card>
      <AlertSnackbar
        open={alertOpen}
        onClose={() => setAlertOpen(false)}
        severity={alertSeverity}
        message={alertMessage}
      />
    </Container>
  );
};

export default AssessmentList;

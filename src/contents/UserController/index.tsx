import React, { useState } from 'react';
import {
  Box,
  Container,
  Tab,
  Tabs,
  Card,
  CardContent,
  TextField,
  Button,
  Stack,
  Divider,
  styled,
  InputAdornment,
  IconButton,
  debounce,
} from '@mui/material';
import AssignSurveys from './AssignSurveys';
import UserApproval from './userApproval';
import ManageUsers from './ManageUsers';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import SearchTwoToneIcon from '@mui/icons-material/SearchTwoTone';
import ClearIcon from '@mui/icons-material/Clear';

const UserController: React.FC = () => {
  const [currentTab, setCurrentTab] = useState<number>(0);
  const [showAddUser, setShowAddUser] = useState<boolean>(false);
  const [email, setEmail] = useState<string>('');
  const [searchKeys, setSearchKeys] = useState<{ [key: number]: string }>({ 0: '', 1: '', 2: '' });
  const { t } = useTranslation();

  const handleTabChange = (event: React.SyntheticEvent, newValue: number) => {
    setCurrentTab(newValue);
    setShowAddUser(false); // Hide the Add User form when changing tabs
  };

  const handleAddUser = async () => {
    try {
      // Mock API call to add user
      console.log('User added:', email);
      alert('User added successfully');
      setEmail('');
      setShowAddUser(false);
    } catch (error) {
      console.error('Error adding user:', error);
      alert('Error adding user. Please try again.');
    }
  };

  const SearchIconWrapper = styled(SearchTwoToneIcon)(
    ({ theme }) => `
          color: ${theme.colors.alpha.black[30]}
  `
  );

  const debouncedHandleSearch = debounce((key: number, value: string) => {
    setSearchKeys((prev) => ({ ...prev, [key]: value }));
  }, 100);

  const handleClearSearch = (key: number) => {
    setSearchKeys((prev) => ({ ...prev, [key]: '' }));
  };

  return (
    <Container maxWidth="lg">
      <Helmet>
        <title>User Management</title>
      </Helmet>
      <Box my={4}>
        <Tabs value={currentTab} onChange={handleTabChange} variant="fullWidth">
          <Tab label="User Management" />
          <Tab label="Approve User" />
          <Tab label="Assign Surveys" />
        </Tabs>
        <Box display="flex" justifyContent="space-between" alignItems="center" my={2}>
          {!showAddUser && (
            <Card
              sx={{
                display: 'flex',
                alignItems: 'center',
                flexGrow: 1,
                marginRight: 2,
              }}
            >
              <Box display={{ xs: 'none', lg: 'flex' }} ml={2} flexShrink={1}>
                <SearchIconWrapper />
              </Box>
              <Stack
                sx={{
                  p: 2,
                  flex: 1,
                }}
                direction={{ xs: 'column', md: 'row' }}
                justifyContent="space-evenly"
                alignItems="center"
                spacing={2}
                divider={<Divider orientation="vertical" flexItem />}
              >
                <TextField
                  sx={{
                    m: 0,
                  }}
                  onChange={(o) => debouncedHandleSearch(currentTab, o.target.value)}
                  placeholder={t('Search by name, email, mobile ...')}
                  value={searchKeys[currentTab]}
                  fullWidth
                  variant="outlined"
                  InputProps={{
                    endAdornment: (
                      <>
                        {searchKeys[currentTab] && (
                          <InputAdornment position="end">
                            <IconButton onClick={() => handleClearSearch(currentTab)} edge="end">
                              <ClearIcon />
                            </IconButton>
                          </InputAdornment>
                        )}
                      </>
                    ),
                  }}
                />
              </Stack>
            </Card>
          )}
          {(currentTab === 0 || currentTab === 1 || currentTab === 2) && (
            <Button
              variant="contained"
              color="primary"
              onClick={() => setShowAddUser(!showAddUser)}
              style={{ height: '50px' }} // Decrease height of the button
            >
              {showAddUser ? 'Cancel' : 'Add User'}
            </Button>
          )}
        </Box>
        {showAddUser ? (
          <Card>
            <CardContent>
              <Box display="flex" alignItems="center">
                <TextField
                  label="Enter Email"
                  variant="outlined"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  fullWidth
                  style={{ marginRight: '16px' }}
                />
                <Button variant="contained" color="primary" onClick={handleAddUser}>
                  Add User
                </Button>
              </Box>
            </CardContent>
          </Card>
        ) : (
          <Card>
            <CardContent>
              {currentTab === 0 && <ManageUsers searchKey={searchKeys[0]} />}
              {currentTab === 1 && <UserApproval searchKey={searchKeys[1]} />}
              {currentTab === 2 && <AssignSurveys searchKey={searchKeys[2]} />}
            </CardContent>
          </Card>
          
        )}
      </Box>
    </Container>
  );
};

export default UserController;

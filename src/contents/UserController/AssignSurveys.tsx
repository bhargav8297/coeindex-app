import React, { useState, useEffect } from 'react';
import {
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Select,
  MenuItem,
  Button,
  ListItemIcon,
  useMediaQuery,
  useTheme,
  TablePagination,
  Skeleton,
} from '@mui/material';
import { Assessment } from '@mui/icons-material';
import _ from 'lodash';
import { getAllSurveyGroups} from 'src/services/apiService'; // Import your API functions
import { Survey } from 'src/services/apiService/response-models';
import axios from 'axios';

interface User {
  id: number;
  Name: string;
  Email: string;
  IsApproved: number;
}

interface AssignSurveysProps {
  searchKey: string;
}

const AssignSurveys: React.FC<AssignSurveysProps> = ({ searchKey }) => {
  const [surveys, setSurveys] = useState<Survey[]>([]);
  const [selectedSurveys, setSelectedSurveys] = useState<Record<number, number>>({});
  const [users, setUsers] = useState<User[]>([]);
  const [loading, setLoading] = useState<boolean>(true); // Loading state
  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(10);
  const theme = useTheme();
  const isSmallScreen = useMediaQuery(theme.breakpoints.down('sm'));

  useEffect(() => {
    const fetchData = async () => {
      try {
        // Fetch surveys
        const surveyRes = await getAllSurveyGroups();
        const surveys = _.flatten(surveyRes.data.map((o) => o.surveys));
        setSurveys(surveys);

        // Fetch users
        const response = await axios.get('https://api-generator.retool.com/mYNBGP/usersData');
        const fetchedUsers = response.data;
        setUsers(fetchedUsers);
        setLoading(false);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []);

  const handleAssign = (userId: number) => {
    const user = users.find((user) => user.id === userId);
    const surveyId = selectedSurveys[userId];
    const survey = surveys.find((survey) => survey.surveyID === surveyId);
    if (user && survey) {
      console.log(`Assigned survey "${survey.surveyName}" to user ${user.Name}`);
      alert(`Assigned survey "${survey.surveyName}" to user ${user.Name}`);
    }
  };

  const filteredUsers = users.filter((user) => user.Name.toLowerCase().includes(searchKey.toLowerCase()));

  const handleSurveyChange = (userId: number, surveyId: number) => {
    setSelectedSurveys((prevState) => ({
      ...prevState,
      [userId]: surveyId,
    }));
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <Box>
      <Table>
        <TableHead>
          <TableRow sx={{ backgroundColor: '#f0f0f0' }}>
            <TableCell sx={{ fontWeight: 'bold' }}>Id</TableCell>
            <TableCell sx={{ fontWeight: 'bold' }}>Name</TableCell>
            <TableCell sx={{ fontWeight: 'bold' }}>Assign Survey</TableCell>
            <TableCell sx={{ fontWeight: 'bold' }}>Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {loading ? ( 
            Array.from(Array(10).keys()).map((index) => (
              <TableRow key={index}>
                <TableCell colSpan={4}>
                  <Skeleton variant="rectangular" height={50} animation="wave" />
                </TableCell>
              </TableRow>
            ))
          ):(
            filteredUsers.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((user) => (
              <TableRow key={user.id} sx={{ '& > *': { padding: '5px' } }}>
                <TableCell>{user.id}</TableCell>
                <TableCell>{user.Name}</TableCell>
                <TableCell>
                  <Select
                    value={selectedSurveys[user.id]} // Use selected survey for this user
                    onChange={(e) => handleSurveyChange(user.id, e.target.value as number)}
                    style={isSmallScreen ? { fontSize: '12px', height: '28px', width: '150px',margin:'10px' } : {}}
                  >
                    {surveys.map((survey) => (
                      <MenuItem key={survey.surveyID} value={survey.surveyID}>
                        <ListItemIcon>
                          <Assessment />
                        </ListItemIcon>
                        {survey.surveyName}
                      </MenuItem>
                    ))}
                  </Select>
                </TableCell>
                <TableCell>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => handleAssign(user.id)}
                    style={isSmallScreen ? { fontSize: '12px', padding: '8px', width: '100%' } : {}}
                  >
                    Assign
                  </Button>
                </TableCell>
              </TableRow>
           ))
          )}
        </TableBody>
      </Table>
      <TablePagination
        component="div"
        count={filteredUsers.length}
        rowsPerPage={
          rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Box>
  );
};

export default AssignSurveys;

// Inside your frontend component
import React, { useState } from 'react';
import { Box, TextField, Button } from '@mui/material';
import axios from 'axios';

const AddUser = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleRegister = async () => {
    try {
      // Send registration data to backend API endpoint
      const response = await axios.post('/api/register', {
        name,
        email,
        password
      });

      // Handle success
      console.log('User registered:', response.data);
      // Provide feedback to the user
      alert('User registered successfully');
    } catch (error) {
      // Handle error
      console.error('Error registering user:', error.response.data);
      // Provide feedback to the user
      alert('Error registering user. Please try again.');
    }
  };

  return (
    <Box>
      <TextField
        label="Name"
        value={name}
        onChange={(e) => setName(e.target.value)}
        fullWidth
        margin="normal"
      />
      <TextField
        label="Email"
        type="email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        fullWidth
        margin="normal"
      />
      <TextField
        label="Password"
        type="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        fullWidth
        margin="normal"
      />
      <Button
        variant="contained"
        color="primary"
        onClick={handleRegister}
      >
        Add User
      </Button>
    </Box>
  );
};

export default AddUser;

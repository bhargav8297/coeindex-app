import React, { useState, useEffect } from 'react';
import {
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Select,
  MenuItem,
  useMediaQuery,
  useTheme,
  Snackbar,
  TablePagination,
  Card,
  CardContent,
  Skeleton
} from '@mui/material';
import { CheckCircleOutline, HourglassEmpty } from '@mui/icons-material';
import axios from 'axios';

interface User {
  id: number;
  Name: string;
  Email: string;
  IsApproved: number;
}

interface UserApprovalProps {
  searchKey: string;
}

const UserApproval: React.FC<UserApprovalProps> = ({ searchKey }) => {
  const [users, setUsers] = useState<User[]>([]);
  const [snackbarOpen, setSnackbarOpen] = useState<boolean>(false);
  const [snackbarMessage, setSnackbarMessage] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(true); // Loading state
  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(10);
  const theme = useTheme();
  const isSmallScreen = useMediaQuery(theme.breakpoints.down('sm'));

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const response = await axios.get('https://api-generator.retool.com/mYNBGP/usersData');
        const fetchedUsers = response.data;
        setUsers(fetchedUsers);
        setLoading(false);
      } catch (error) {
        console.error('Error fetching users:', error);
      }
    };

    fetchUsers();
  }, []);

  const handleStatusChange = async (userId: number, newStatus: string) => {
    try {
      setUsers((prevUsers) =>
        prevUsers.map((user) =>
          user.id === userId ? { ...user, IsApproved: newStatus === 'Approved' ? 1 : 0 } : user
        )
      );

      const userToUpdate = users.find((user) => user.id === userId);
      if (!userToUpdate) return;

      await axios.put(`https://api-generator.retool.com/mYNBGP/usersData/${userId}`, {
        ...userToUpdate,
        IsApproved: newStatus === 'Approved' ? 1 : 0,
      });

      setSnackbarMessage(`User ${userId} status changed to ${newStatus}`);
      setSnackbarOpen(true);
    } catch (error) {
      console.error('Error updating status:', error);
    }
  };

  const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const filteredUsers = users.filter(
    (user) =>
      user.Name.toLowerCase().includes(searchKey.toLowerCase()) ||
      user.Email.toLowerCase().includes(searchKey.toLowerCase())
  );

  return (
    <>
    <Box>
      <Table>
        <TableHead>
          <TableRow sx={{ backgroundColor: '#f0f0f0' }}>
            <TableCell sx={{ fontWeight: 'bold' }}>Id</TableCell>
            <TableCell sx={{ fontWeight: 'bold' }}>Name</TableCell>
            <TableCell sx={{ fontWeight: 'bold' }}>Email</TableCell>
            <TableCell sx={{ fontWeight: 'bold' }}>Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {loading ? (
            Array.from(Array(10).keys()).map((index) => (
              <TableRow key={index}>
                <TableCell colSpan={4}>
                  <Skeleton variant="rectangular" height={50} animation="wave" />
                </TableCell>
              </TableRow>
            ))
          ):(filteredUsers.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((user) => (
            <TableRow key={user.id} sx={{ '& > *': { padding: '3px' } }}>
              <TableCell>{user.id}</TableCell>
              <TableCell>{user.Name}</TableCell>
              <TableCell>{user.Email}</TableCell>
              <TableCell>
                <Select
                  value={user.IsApproved === 0 ? 'Pending' : 'Approved'}
                  onChange={(e) => handleStatusChange(user.id, e.target.value as string)}
                  sx={{ width: isSmallScreen ? '100%' : '150px',margin:'10px' }}
                >
                  <MenuItem sx={{ color: '#ff9800', display: 'flex', alignItems: 'center' }} dense value="Pending">
                    <HourglassEmpty sx={{ verticalAlign: 'middle', marginRight: 1 }} /> <span style={{ color: '#ff9800' }}>Pending</span>
                  </MenuItem>
                  <MenuItem sx={{ color: '#4caf50', display: 'flex', alignItems: 'center' }} dense value="Approved">
                    <CheckCircleOutline sx={{ verticalAlign: 'middle', marginRight: 1 }} /> <span style={{ color: '#4caf50' }}>Approved</span>
                  </MenuItem>
                </Select>
              </TableCell>
            </TableRow>
            ))
          )}
        </TableBody>
      </Table>
      <Snackbar
        open={snackbarOpen}
        onClose={() => setSnackbarOpen(false)}
        message={snackbarMessage}
        autoHideDuration={3000}
      />
    </Box>
    <Box mt={2} />
      <TablePagination
        component="div"
        count={filteredUsers.length}
        page={page}
        onPageChange={handleChangePage}
        rowsPerPage={rowsPerPage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </>
  );
};

export default UserApproval;

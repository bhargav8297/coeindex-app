import { useContext } from "react";
import AuthContext from "src/contexts/Auth0Context";

const ApiBaseUrl = process.env.REACT_APP_COE_INDEX_SERVICE_ENDPOINT || '';
const PdfBlobBaseUrl = process.env.REACT_APP_PDF_BLOB_URL || '';


export const ApiUrls = {
  base: ApiBaseUrl,
  surveyEndpoint: ApiBaseUrl + '/api/v1/survey',
  membershipEndpoint: ApiBaseUrl + '/api/v1/membership',
  stripePaymentSuccessReturnUrl: process.env.REACT_APP_STRIPE_RETURN_URL || '',
  pdfBlobEndpoint: PdfBlobBaseUrl
};